package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    private int rows;
    private int column;
    public int[][] buildPyramid(List<Integer> input) {
            int[][] pyramid = new int[0][0];
            try {
                for (int i = 0; i < input.size(); i++) {
                    if (input.get(i) == null) {
                        throw new CannotBuildPyramidException();
                    }
                }
                if (input.size() == Integer.MAX_VALUE - 1) {
                    throw new CannotBuildPyramidException();
                }
                int[] array = input.stream().mapToInt(i->i).toArray();
                if (check(array.length) == 1) {
                    column = 2 * rows - 1;
                    pyramid = new int[rows][column];
                } else {
                    throw new CannotBuildPyramidException();
                }
                for (int i = array.length - 1; i > 0; i--) {
                    for (int j = 0; j < i; j++) {
                        if (array[j] > array[j + 1]) {
                            int tmp = array[j];
                            array[j] = array[j + 1];
                            array[j + 1] = tmp;
                        }
                    }
                }
                for (int i = 0; i < rows; i++) {
                    for (int j = 0; j < column; j++) {
                        pyramid[i][j] = 0;
                    }
                }
                int k = 0;
                int a = column / 2 + 1;
                for (int i = 0; i < rows; i++) {
                    a--;
                    int l = a;
                    for (int j = 0; j < (i + 1); j++) {
                        pyramid[i][l] = array[k];
                        k++;
                        l += 2;
                    }
                }
            }
            catch (CannotBuildPyramidException e) {
                throw e;
            }
            return pyramid;
    }
    public int check (int n) {
        int sum = 0;
        int i = 1;
        if(n < 3)
            return 0;
        while(sum < n){
            sum += i;
            i++;
        }
        if(sum == n) {
            rows = i - 1;
            return 1;
        }
        return 0;
    }

}
